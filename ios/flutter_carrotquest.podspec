#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutter_carrotquest.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_carrotquest'
  s.version          = '0.0.4'
  s.summary          = 'A new flutter plugin project.'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'https://dipdev.studio'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'DipDev Studio' => 'info@dipdev.studio' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '13.0'
  
  # CarrotSDK does not support arm64 simulators.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.dependency 'CarrotquestSDK', '~> 2.3.0'
end
