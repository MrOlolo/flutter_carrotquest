import CarrotSDK
import Flutter
import UIKit

public class SwiftFlutterCarrotquestPlugin: NSObject, FlutterPlugin {
    var pluginInited = false
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutter_carrotquest", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterCarrotquestPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let arguments = call.arguments as? NSDictionary
        switch call.method {
        case "setup":
            if pluginInited {
                result(nil)
                return
            }
            guard let apiKey = arguments?["api_key"] as? String else {
                result(
                    FlutterError(code: "An error has occurred, the apiKey is null.",
                                 message: nil,
                                 details: nil))
                return
            }
            Carrot.shared.setup(
                withApiKey: apiKey,
                successHandler: {
                    self.pluginInited = true
                    result(nil)
                },
                errorHandler: { error in
                    result(
                        FlutterError(code: error,
                                     message: nil,
                                     details: nil))
                })

        case "set_debug":
            result(FlutterMethodNotImplemented)

        case "auth":
            if !checkPluginInitialization(result: result) {
                return
            }
            guard let userAuthKey = arguments?["user_auth_key"] as? String else {
                result(
                    FlutterError(code: "An error has occurred, the userAuthKey is null.",
                                 message: nil,
                                 details: nil))
                return
            }
            guard let userId = arguments?["user_id"] as? String else {
                result(
                    FlutterError(code: "An error has occurred, the userId is null.",
                                 message: nil,
                                 details: nil))
                return
            }
            Carrot.shared.auth(
                withUserId: userId,
                withUserAuthKey: userAuthKey,
                successHandler: {
                    result(nil)
                },
                errorHandler: { error in
                    result(
                        FlutterError(code: error,
                                     message: nil,
                                     details: nil))
                })

        case "set_user_properties":
            if !checkPluginInitialization(result: result) {
                return
            }
            guard let properties = arguments?["user_properties"] as? [String: String] else {
                result(
                    FlutterError(code: "An error has occurred, properties is null.",
                                 message: nil,
                                 details: nil))
                return
            }
            var userProperties: [UserProperty] = []

            for (k, v) in properties {
                userProperties.append(UserProperty(key: k, value: v))
            }
            Carrot.shared.setUserProperty(userProperties)
            result(nil)

        case "set_user_properties_with_operations":
            if !checkPluginInitialization(result: result) {
                return
            }
            guard let properties = arguments?["user_properties"] as? [[String: String]] else {
                result(
                    FlutterError(code: "An error has occurred, properties is null.",
                                 message: nil,
                                 details: nil))
                return
            }
            var userProperties: [UserProperty] = []

            for prop in properties {
                userProperties.append(UserProperty(key: prop["key"] ?? "n/a",
                                                   value: prop["value"] ?? "n/a",
                                                   operation: userPropertyOperationFromString(str: prop["operation"]))
                )
            }
            Carrot.shared.setUserProperty(userProperties)
            result(nil)

        case "de_init":
            if !checkPluginInitialization(result: result) {
                return
            }
            result(FlutterMethodNotImplemented)

        case "open_chat":
            if !checkPluginInitialization(result: result) {
                return
            }
            Carrot.shared.openChat()
            result(nil)

        case "set_fcm_token":
            if !checkPluginInitialization(result: result) {
                return
            }
            guard let fcmToken = arguments?["fcm_token"] as? String else {
                result(
                    FlutterError(code: "An error has occurred, the fcmToken is null.",
                                 message: nil,
                                 details: nil))
                return
            }
            CarrotNotificationService.shared.setToken(fcmToken)
            result(nil)

        case "logout":
            Carrot.shared.logout(successHandler: {
                    result(nil)
                },
                errorHandler: { error in
                    result(
                        FlutterError(code: error,
                                     message: nil,
                                     details: nil))
                })

        default:
            result(FlutterMethodNotImplemented)
        }
    }

    private func checkPluginInitialization(result: @escaping FlutterResult) -> Bool {
        if !pluginInited {
            result(
                FlutterError(code: "The plugin hasn't been initialized yet. Do Carrot.setup(...) first.",
                             message: nil, details: nil))
            return false
        }
        return true
    }

    private func userPropertyOperationFromString(str: String?) -> UserProperty.Operations {
        switch str {
        case "setOnce":
            return UserProperty.Operations.setOnce
        case "add":
            return UserProperty.Operations.add
        case "union":
            return UserProperty.Operations.union
        case "append":
            return UserProperty.Operations.append
        case "delete":
            return UserProperty.Operations.delete
        case "exclude":
            return UserProperty.Operations.exclude
        default:
            return UserProperty.Operations.updateOrCreate
        }
    }
}
