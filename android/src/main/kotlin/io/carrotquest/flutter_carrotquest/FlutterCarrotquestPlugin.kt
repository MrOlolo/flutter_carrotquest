package io.carrotquest.flutter_carrotquest

import android.app.Activity
import android.content.Context
import androidx.annotation.NonNull
import io.carrotquest_sdk.android.Carrot
import io.carrotquest_sdk.android.core.main.CarrotSDK
import io.carrotquest_sdk.android.models.UserProperty
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

class FlutterCarrotquestPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {
    private lateinit var context: Context
    private var channel: MethodChannel? = null
    private var activity: Activity? = null

    override fun onAttachedToEngine(
            @NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        context = flutterPluginBinding.applicationContext
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_carrotquest")
        channel?.setMethodCallHandler(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "setup" -> {
                if (Carrot.isInit()) {
                    result.success(null)
                    return
                }
                val apiKey = call.argument<String>("api_key")
                val appId = call.argument<String>("app_id")
                if (apiKey == null || appId == null) {
                    result.error("An error has occurred, the apiKey or appId is null.", null, null)
                    return
                }
                Carrot.setup(context, apiKey, appId, object : CarrotSDK.Callback<Boolean> {
                    override fun onFailure(p0: Throwable?) {
                        result.error(p0.toString(), null, null)
                    }

                    override fun onResponse(p0: Boolean?) {
                        result.success(null)
                    }
                })
            }
            "set_debug" -> {
                var isDebug = call.argument<Boolean>("is_debug")
                if (isDebug == null) isDebug = true
                try {
                    Carrot.setDebug(isDebug)
                    result.success(null)
                } catch (e: Exception) {
                    result.error(e.localizedMessage, null, null)
                }
            }
            "auth" -> {
                if (!checkPluginInitialization(result)) return
                val userId = call.argument<String>("user_id")
                val userAuthKey = call.argument<String>("user_auth_key")
                if (userId == null || userAuthKey == null) {
                    result.error("An error has occurred, the userId or userAuthKey is null.", null, null)
                    return
                }
                Carrot.auth(userId, userAuthKey, object : CarrotSDK.Callback<Boolean> {
                    override fun onResponse(p0: Boolean?) {
                        result.success(p0)
                    }

                    override fun onFailure(p0: Throwable?) {
                        result.error(p0?.localizedMessage, null, null)
                    }
                })
            }
            "set_user_properties" -> {
                if (!checkPluginInitialization(result)) return
                val properties = call.argument<Map<String, String>?>("user_properties")
                if (properties == null || properties.isEmpty()) {
                    result.error("An error has occurred, the properties is empty.", null, null)
                    return
                }
                val userProperties = mutableListOf<UserProperty>()

                try {
                    for ((k, v) in properties) userProperties.add(UserProperty(k, v))
                    Carrot.setUserProperty(userProperties)
                    result.success(null)
                } catch (e: Exception) {
                    result.error(e.localizedMessage, null, null)

                }

            }
            "set_user_properties_with_operations" -> {
                if (!checkPluginInitialization(result)) return
                val properties = call.argument<List<Map<String, String>>?>("user_properties")
                if (properties == null || properties.isEmpty()) {
                    result.error("An error has occurred, the properties is empty.", null, null)
                    return
                }
                val userProperties = mutableListOf<UserProperty>()

                try {
                    for (prop in properties) {
                        userProperties.add(
                                UserProperty(
                                        userPropertyOperationFromString(prop["operation"]),
                                        prop["key"],
                                        prop["value"]),
                        )
                    }
                    Carrot.setUserProperty(userProperties)
                    result.success(null)
                } catch (e: Exception) {
                    result.error(e.localizedMessage, null, null)

                }

            }
            "de_init" -> {
                if (!checkPluginInitialization(result)) return

                Carrot.deInit(object : CarrotSDK.Callback<Boolean> {
                    override fun onResponse(p0: Boolean?) {
                        result.success(p0)
                    }

                    override fun onFailure(p0: Throwable?) {
                        result.error(p0?.localizedMessage, null, null)
                    }
                })

            }
            "open_chat" -> {
                if (!checkPluginInitialization(result)) return
                try {
                    if (activity != null) {
                        Carrot.openChat(activity!!)
                        result.success(null)
                    } else {
                        result.error("Activity is null", null, null)
                    }

                } catch (e: Exception) {
                    result.error(e.localizedMessage, null, null)
                }
            }
            "is_init" -> {
                result.success(Carrot.isInit())
            }
            "set_fcm_token" -> {
                result.notImplemented()
            }
            "logout" -> {
                result.notImplemented()
            }
            else -> {
                result.notImplemented()
            }
        }


    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel?.let {
            it.setMethodCallHandler(null)
            channel = null
        }
    }

    override fun onDetachedFromActivity() {
        activity = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        activity = binding.activity
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity
    }


    override fun onDetachedFromActivityForConfigChanges() {
        activity = null
    }

    private fun checkPluginInitialization(@NonNull result: Result): Boolean {
        if (!Carrot.isInit()) {
            result.error("The plugin hasn't been initialized yet. Do Carrot.setup(...) first .", null, null)
            return false
        }
        return true
    }

    private fun userPropertyOperationFromString(str: String?): UserProperty.Operation {
        when (str) {
            "setOnce" -> {
                return UserProperty.Operation.SET_ONCE
            }
            "add" -> {
                return UserProperty.Operation.ADD
            }
            "union" -> {
                return UserProperty.Operation.UNION
            }
            "append" -> {
                return UserProperty.Operation.APPEND
            }
            "delete" -> {
                return UserProperty.Operation.DELETE
            }
            "exclude" -> {
                return UserProperty.Operation.EXCLUDE
            }
            else -> {
                return UserProperty.Operation.UPDATE_OR_CREATE
            }

        }
    }
}
