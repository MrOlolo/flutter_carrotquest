import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter_carrotquest/flutter_carrotquest.dart';

const cqAuthKey = '';
const cqApiKey = '';
const cqAppId = '';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ///Init carrot quest
  await Carrot.setup(apiKey: cqApiKey, appId: cqAppId)
      .catchError((onError) => print(onError));
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ///Change to use another id at auth when app started
  String initUserId = 'ypurd21edawwad';

  ///Change to use another id at auth when press 'login' button
  String buttonUserId = 'your userId2';

  String lastError;

  bool isAuthorized = false;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    //  await Carrot.setDebug().catchError((onError) => print(onError));
    try {
      ///Auth carrot quest
      await Carrot.auth(userId: initUserId, userAuthKey: cqAuthKey);
      setState(() {
        isAuthorized = true;
      });
    } catch (onError) {
      setState(() {
        lastError = onError.toString();
      });
      print(onError);
    }
    //
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          label: Text('Open Chat'),
          onPressed: () async {
            await Carrot.openChat().catchError((onError) {
              setState(() {
                lastError = onError.toString();
              });
              print(onError);
            });
          },
        ),
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              Text('Status: ${isAuthorized ? 'Authorized' : 'Not authorized'}'),
              SizedBox(
                height: 20,
              ),
              Text('Last catched error: $lastError'),
              TextButton(
                  onPressed: () async {
                    try {
                      await Carrot.auth(
                          userId: buttonUserId, userAuthKey: cqAuthKey);
                      setState(() {
                        isAuthorized = true;
                      });
                    } catch (onError) {
                      setState(() {
                        lastError = onError.toString();
                      });
                      print(onError);
                    }
                  },
                  child: Text('Login')),
              TextButton(
                  onPressed: () async {
                    await Carrot.logout();
                    setState(() {
                      isAuthorized = false;
                    });
                  },
                  child: Text('Logout')),
            ],
          ),
        ),
      ),
    );
  }
}
