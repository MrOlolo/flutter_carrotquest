import 'package:flutter/foundation.dart';

enum UserPropertyOperation {
  add,
  union,
  append,
  updateOrCreate,
  delete,
  exclude,
  setOnce,
}

class UserProperty {
  final String key;
  final String value;
  final UserPropertyOperation operation;

  UserProperty(
      {required this.key,
      required this.value,
      this.operation = UserPropertyOperation.updateOrCreate});

  Map<String, String> toJson() => {
        'key': key,
        'value': value,
        'operation': describeEnum(operation),
      };
}
